package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/userinMongo/domain"
	"gitlab.com/userinMongo/services"
	"gitlab.com/userinMongo/utils"
	"net/http"
	"strconv"
)

func GetUser(context *gin.Context) {
	userId, err := strconv.ParseUint(context.Param("user_id"), 10, 64)
	if err != nil {
		apiErr := &utils.ApplicationError{
			Message:    "user_id must be number",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(context, apiErr)
	}
	user, apiErr := services.UserService.GetUser(userId)
	if apiErr != nil {
		utils.RespondError(context, apiErr)
		return
	}
	utils.Respond(context, http.StatusOK, user)
}
func CreateUser(context *gin.Context) {
	var user domain.User
	err := context.ShouldBindJSON(&user)
	if err != nil {
		restErr := &utils.ApplicationError{
			Message:    "invalid json body",
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
		utils.RespondError(context, restErr)
	}
	saveErr := services.UserService.CreateUser(&user)
	if saveErr != nil {
		utils.RespondError(context, saveErr)
		return
	}
	utils.Respond(context, http.StatusCreated, "new user added")
}
