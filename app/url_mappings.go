package app

import (
	"gitlab.com/userinMongo/controllers/ping"
	"gitlab.com/userinMongo/controllers/user"
)

func mapUrls() {
	router.GET("/ping", ping.Ping)
	router.POST("/users", user.CreateUser)
	router.GET("/users/:user_id", user.GetUser)

}
