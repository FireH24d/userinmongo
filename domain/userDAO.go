package domain

import (
	"context"
	"fmt"
	"gitlab.com/userinMongo/db"
	"gitlab.com/userinMongo/utils"
	"go.mongodb.org/mongo-driver/bson"
	"net/http"
)

var (
	UserDao userDaoInterface
)

func init() {
	UserDao = &userDao{}
}

type userDaoInterface interface {
	GetUser(uint64) (*User, *utils.ApplicationError)
	CreateUser(*User) *utils.ApplicationError
}
type userDao struct{}

func (*userDao) GetUser(userId uint64) (*User, *utils.ApplicationError) {
	client, err := db.GetClient()
	if err != nil {
		return nil, err
	}
	collection := client.Database("userdb").Collection("users")
	document := collection.FindOne(context.TODO(), bson.M{"id": userId})
	var user User
	err2 := document.Decode(&user)
	if err2 != nil {
		return nil, &utils.ApplicationError{
			Message:    fmt.Sprintf("user %v cannot be created", userId),
			StatusCode: http.StatusNotFound,
			Code:       "not_found",
		}
	}
	return &user, nil
}

func (*userDao) CreateUser(user *User) *utils.ApplicationError {
	client, err := db.GetClient()
	if err != nil {
		return err
	}
	collection := client.Database("userdb").Collection("users")
	_, err2 := collection.InsertOne(context.TODO(), user)
	if err2 != nil {
		return &utils.ApplicationError{
			Message:    fmt.Sprintf("user %v cannot be created", user.ID),
			StatusCode: http.StatusBadRequest,
			Code:       "bad_request",
		}
	}
	return nil
}
