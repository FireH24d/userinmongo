package services

import (
	"gitlab.com/userinMongo/domain"
	"gitlab.com/userinMongo/utils"
)

type userService struct {
}

var (
	UserService userService
)

func (*userService) GetUser(userId uint64) (*domain.User, *utils.ApplicationError) {
	user, err := domain.UserDao.GetUser(userId)
	if err != nil {
		return nil, err
	}
	return user, nil
}
func (*userService) CreateUser(user *domain.User) *utils.ApplicationError {
	err := domain.UserDao.CreateUser(user)
	if err != nil {
		return err

	}
	return nil

}
